//
//  OutlookCalendarReader.swift
//  MHGOAuthDemo
//
//  Created by RuiJiankun on 5/12/2015.
//  Copyright © 2015 Heguang Miao. All rights reserved.
//

import Foundation

import Foundation
import MHGOAuth_Swift

class OutlookCalendarReader : CalendarReadable {
    var tokenBundle:TokenBundle
    
    init(tokenBundle:TokenBundle){
        self.tokenBundle = tokenBundle
    }
    
    func readCalendar(callback: (error:NSError?, response:NSData?) -> Void) {
        let request = NSMutableURLRequest(URL: NSURL(string: "https://outlook.office.com/api/v2.0/me/events")!)
        request.addValue(tokenBundle.accessToken, forHTTPHeaderField: "authorization")
        let sessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        sessionConfiguration.requestCachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        
        let session = NSURLSession(configuration: sessionConfiguration)
        let task = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) in
            if let error = error {
                callback(error: error, response: nil)
            } else {
                callback(error: nil,
                    response: data)
            }
        })
        task.resume()
    }
}