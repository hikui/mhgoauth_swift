//
//  CalendarReader.swift
//  MHGOAuthDemo
//
//  Created by Heguang Miao on 26/11/2015.
//  Copyright © 2015 Heguang Miao. All rights reserved.
//

import Foundation
import MHGOAuth_Swift

public protocol CalendarReadable {
    var tokenBundle:TokenBundle{get set}
    func readCalendar(callback:(error:NSError?, response:NSData?)->Void)
}