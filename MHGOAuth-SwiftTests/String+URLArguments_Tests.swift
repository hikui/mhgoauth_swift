//
//  String+URLArguments_Tests.swift
//  MHGOAuth_Swift
//
//  Created by Heguang Miao on 26/11/2015.
//  Copyright © 2015 Heguang Miao. All rights reserved.
//

import XCTest

class String_URLArguments_Tests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testEncoding() {
        let e = "abc+def".stringByEscapingForURLArgument()
        print(e)
        XCTAssertEqual(e, "abc+def")
    }

}
